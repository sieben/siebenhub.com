---
title: Resume
layout: page
---

&#114;&#101;&#109;&#121;&#46;&#108;&#101;&#111;&#110;&#101;&#64;&#103;&#109;&#97;&#105;&#108;&#46;&#99;&#111;&#109;


## Education

- **Master's Degree in Computer Science and Research at
  [UPMC](//upmc.fr)** _(Sept 2011 to present)_
> Networks with an emphasis on security, Quality of Service,
> Carrier-Grade networks.

>> _Electives :_ Graphs, Distributed Resilience to Attacks, Advanced
>> Security.

- **Engineering degree from [ENSIIE](//ensiie.fr)** _(Sept 2009 to
  present)_
> Design techniques, modeling, programming, combined with business
> management, economics, project management, ensuring good versatility.

>> _Electives :_ Operating Systems, Local Area Networks Administration,
>> Security, Concurrent Programming, Networks Quality of Service.

## Professional Experience

### [Telecom ParisTech](//telecom-paristech.fr), Paris

#### Study engineer - October 2012 to December 2012

> - Implementation of the [CoAP](//tools.ietf.org/html/draft-ietf-core-coap)
> protocol in a [Python](//python.org) library.
> - Implementation of control web interface for a [Wifibot](//wifibot.com/)
> controlled by HTTP and [CoAP](//tools.ietf.org/html/draft-ietf-core-coap).

> _Technical environment :_ [Python](//python.org), [Flask](//flask.pocoo.org/)

### [Thales](//www.thalesgroup.com), Colombes

#### Study engineer - April 2012 to September 2012

> - Study of protocols related to Wireless Sensors Networks such as
> [CoAP](//tools.ietf.org/html/draft-ietf-core-coap) and
> [RPL](//tools.ietf.org/html/rfc6550).
> - Implementation of a cache designed to save energy in
> publish/subscribe and end-to-end communications scenarios.
> - Development of Contiki applications and middleware in Wireless Sensor Network.

> _Technical environment :_ [Contiki](//contiki-os.org), Java, C,
> REST, CoAP, [CouchDB](//couchdb.apache.org).

### [Paris-Montagne](//paris-montagne.org/), [ENS Ulm](//www.ens.fr/), Paris

#### Software developer - June 2011 to September 2011

> - Programmed a geolocation tool in order to provide statistical information
> about geographic repartition of students.
> - Used free software and a free geographic licence in order
> to respect privacy of the users ([geoDjango](//geodjango.org/),
> [PostGIS](//postgis.org),
> [OpenStreetMap](//openstreetmap.org/)).

> _Technical environment :_ [GNU/Linux Debian](//debian.org),
> [Shell](//zsh.org), [SSH](//openssh.org), [gunicorn](//gunicorn.org),
> [PostgreSQL](//postgresql.org), [PostGis](//postgis.org), HTML, CSS,
> [javascript](//developer.mozilla.org/fr/javascript),
> [Leaflet](//leafletjs.com),
> [OpenStreetMap](//openstreetmap.org), [git](//git-scm.org),
> [jQuery](//jquery.org), [JSON](//json.org), AJAX,
> [nginx](//nginx.com).

### [CEA DAM](//www-dam.cea.fr), Bruyères-le-Châtel

#### Scientific code refactoring - June 2010 to September 2010

> - Refactored of a Fortran nuclear physics application from a Sun
> operating system to a Red Hat Entreprise Linux.

> _Technical environment :_ gfortran, Sun OS, Red Hat Entreprise Linux,
> Shell.

## Skills

**Programming Languages:**

> [Python](//python.org),
> [javascript](//developer.mozilla.org/fr/javascript),
> [Shell](//zsh.org), C,
> [C++](//www.open-std.org/jtc1/sc22/wg21/),
> Java, [Ocaml](//ocaml.org),
> Assembly,[Fortran](//www.nag.co.uk/sc22wg5/)

**Frameworks:**

> [Django](//djangoproject.com), [geoDjango](//geodjango.org),
> [jQuery](//jquery.org)

**Database management systems:**

> [MySQL](//mysql.com), [PostgreSQL](//postgresql.org),
> [PostGIS](//postgis.org)

**Operating systems:**

> - Windows & MacOSX (user-mode practice)
> - GNU/Linux (system administration notions, development experience)
> - [Contiki & Cooja](//contiki-os.org)

**Open formats:**

> [HTML](//www.w3.org/TR/html-markup/), [CSS](//www.w3.org/Style/CSS/),
> [YAML](//www.yaml.org/spec/), [JSON](//json.org),
> [XML](http://www.w3.org/TR/2006/REC-xml11-20060816/),
> [LaTeX](//tug.org), [XeTeX](//tug.org/xetex)

**Coding tools:**

> [Eclipse](//eclipse.org), [vim](//vim.org), [git](//git-scm.org),
> [Subversion](//subversion.tigris.org)

**Languages:**

- *French* - Native Speaker
- *English* - Fluent
- *Spanish* - Working knowledge

## Associative Experience

### [Paris-Montagne](//paris-montagne.org/), [ENS Ulm](//www.ens.fr/) - Paris

#### Volunteer position - Every summer since 2006

The purpose of this association is to show how the world of scientific
research works. Our target audience predominantly consists of high
school students to whom we offer educational activities. I was running a
stand during a scientific festival giving practical information on high
technology at ENS Ulm.

### [Jardin Experimental](//jardin-experimental.com/), [Cité des Sciences et de l'Industrie](//cite-sciences.fr) - Paris

#### Scientific Presenter - 2009

I was running a stand about the new technologies in la Cité des Sciences
et de l’Industrie. The Cité des Sciences et de l'Industrie is the
biggest science museum in Europe. Located in Parc de la Villette in
Paris, France, it is at the heart of the Cultural Center of Science,
Technology and Industry (CCSTI), a center promoting science and science
culture.

### [Summer School of Science](//s3.sci.hr/), Višnjan - Croatia

#### International research internship - Summer 2007

Scientific research in English, dealing with the computational chemistry
of a Cubane molecule.  This experience introduced me to foreign
scientists and helped me finetune my English by giving presentations.
