---
title: Current projects
layout: page
---

I currently work on:

* [Cerebro (work in progress, not deployed yet)](http://cerebro.sieben.fr)

  Geolocation of students information. This project aims to make the
action of the association
[Paris-montagne](http://www.paris-montagne.org) more visible. Any help
on front side (HTML/CSS/JS) or on the Django part is more than welcome.
