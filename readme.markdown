Sieben.fr | My website
======================

[![Build Status](https://secure.travis-ci.org/sieben/sieben.github.com.png)](http://travis-ci.org/sieben/sieben.github.com)

- Me faire un logo classe
- Mettre des couleurs genre rouge et noir
- Ajouter une partie pour mes présentation en full HTML5 [https://github.com/n1k0/landslide](https://github.com/n1k0/landslide)
- Ajouter mes centres d'intérêt brief outline
- research bibliography
- Réparer les problèmes de résolution sur mobile et tablette
- Alegreya SC comme font
- Utiliser plus de CDN dans ce style : http://www.bootstrapcdn.com/
- Utiliser des couleurs plus classe comme : http://www.lavishbootstrap.com/
