---
layout: page
title: About me
---

Philosophy
==========

My philosophy on software and life is the same: be agile. I strongly
believe in learning from mistakes, iterating quickly, and building
strength over time. I understand that big change is made from consistent
small efforts, and strive to continuously improve myself every day.

When I work on things, I give them 100% of my effort. Regardless of
whether I am folding laundry or writing unit tests, I thoroughly enjoy
myself and constantly push myself to new levels of understanding and
efficiency.


Purpose
=======

I want to teach, I don't care what, I don't care with who, I don't care
if it's well paid. That's what I like and what I want to do with my
life.


Personality
============

- I'm very patient
- I read about science, software, and computers everyday thanks to my
Google Reader.

Basically, (I use this word a lot according to my friends) I'm friendly, fun and very nerdy :)


Achievements
============

![Project Euler ranking](//projecteuler.net/profile/sieben.png)

Why sieben?
===========

- 7 is a prime number.
- 007 is classy.
- I like German (even if I don't speak it).
- It's short for a domain name and can be type quickly.
