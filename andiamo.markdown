---
title: Andiamo - Cartes des pizzas &agrave; Andiamo &agrave; port&eacute;e de main
layout: page
---

CONTACT
-------

Par téléphone au 01 60 79 30 30

PIZZAS
------

<table>
<tr valign="top">
<td></td>
<td></td>
<td align="right"><b>Junior</b></td>
<td align="right"><b>Super</b></td>
<td align="right"><b>Mega</b><br></td>
</tr>
<tr valign="top">
<td>Classica</td>
<td>Tomate, fromage</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">16,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">21,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Torino</td>
<td>T, F + champignons, jambon</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">16,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">21,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Bergama</td>
<td>T, F + poulet, champignons, oeuf</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">16,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">21,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Vesuvio</td>
<td>T, F + jambon, oeuf (souffl&eacute;e)</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">16,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">21,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Napolitaine</td>
<td>T, F + thon, olives, anchois, c&acirc;pres</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">16,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">21,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td></td>
<td></td>
<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Capri</td>
<td>T, F + cocktail de fruits de mer</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">17,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">23,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Roma</td>
<td>T, F + merguez, oignons, champignons, oeuf</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">17,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">23,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Calabria</td>
<td>T, F + merguez, viande hach&eacute;e, poivrons, oignons</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">17,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">23,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Sicilienne</td>
<td>T, F + v. hach&eacute;e, champis, poivrons, tomates
fra&icirc;ches</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">17,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">23,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Mama Lucia</td>
<td>T, F + merguez, oignons, piment</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">17,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">23,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Cilento</td>
<td>T, F + v. hach&eacute;e, oignons, champignons</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">17,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">23,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>San Pietro</td>
<td>T, F + merguez, chorizo, oeuf, jambon</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">17,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">23,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td></td>
<td></td>
<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Rimini</td>
<td>Cr&egrave;me fra&icirc;che, fromage, poulet, pommes de
terre</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">17,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">23,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Sardegna</td>
<td>CF, F + assortiment de 4 fromages</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">17,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">23,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Venezia</td>
<td>CF, F + saumon fum&eacute;</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">17,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">23,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Milano</td>
<td>CF, F + jambon, oignons, pommes de terre</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">17,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">23,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Lolita</td>
<td>CF, F + jambon, ananas</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">17,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">23,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
<tr valign="top">
<td>Florentina</td>
<td>CF, F + viande hach&eacute;e, oignons, oeuf</td>
<td align="right">8,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">17,00 &nbsp;&nbsp;&nbsp;&nbsp;</td>
<td align="right">23,00&nbsp;&nbsp;&nbsp;&nbsp;<br></td>
</tr>
</table>
