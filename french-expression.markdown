---
title: Funny French idioms.
layout: page
---

Format : French idioms, English translation, word by word translation

Anger
-----

- Péter les plombs, to blow one's top, Bust the fuses (Here the word "plombs"
refers to the fuses that melt in order to avoid short-circuit)

- Sortir de ses gonds, to fly off the handle, (The image of a door that come
  off its hinges)

- Se fâcher tout rouge, to go up the wall, to be upset all red

- J'ai la moutarde qui me monte au nez, I'm beginning to lose my temper,
  Mustard is winding up in my nose.

- Prendre la mouche, to fly off the handle, Take a fly (This expression is
  close to "quelle mouche vous a piqué ?/What's with you?") The idea is that a
  person's sudden outburst is the result of having being stung by an insect.

- Avoir les nerfs en pelote, To be on the edge, to be nervy, Got my nerves in a
  ball (like ball of wool)

Criticizing
-----------

- Casser du sucre sur le dos de quelqu'un, to bad-month someone, break sugar on
  the back of someone

- Tailler un costard à quelqu'un, to slag someone off, Cut a suit for someone
  (The idea here is that once you have a bad reputation it's difficult to lose
  it, in the same way that a suit cannot be altered once it has been cut to a
  certain shape)

Annoyance
---------

- Casser les pieds de quelqu'un, to do someone's head in, Break the feet of
  someone.

Getting along (or not)
----------------------

- S'entendre comme larrons en foire, to be as thick as thieves, agreeing as
  thieves in a fair

- Être comme cul et chemise, to be inseparable, Be like ass and shirt (more for
  a friendship than a love)

- Être comme les doigts de la main, to be very close, Be like fingers of the
  hand.

- Il me sort par les trous de nez, I can't stand the sight of him, He is
  getting out of my nostrils.

- Entre eux le torchon brule, They're at loggerheads, The rag is burning
  between them.

Love
----

- Avoir le coup de foudre, To fall in love at first sight, to be stuck by
  lightning.

- Être un bourreau des coeurs, to be a ladykiller, Being a executioner of
  hearts.

- Avoir un coeur d'artichaut, to be a fickle (in love), To have a artichoke
  heart (A leaf for everyone)

- Avoir quelqu'un dans la peau, to be crazy about someone, having someone in
  the skin.

- Être mordu, to be madly in love, to be beaten

- En pincer pour quelqu'un, to be crazy about someone, To pinch for someone.

Truth and lies
--------------

- Mentir comme un arracheur de dents, to be a compulsive liar, Lie as a teeth
  puller (no anaesthetic but don't worry you will not feel pain)

- Raconter des salades, to tell fibs, Tell salads

- Faire avaler des couleuvres, to take someone in, Make somebody swallow
  grass-snake

- Une histoire à dormir debout, A cock and bull story, A story that make you
  sleep while standing up.

- Se mettre sur son 31, to get all dressed up, To get on his/her 31 (31 as
  December 31th)

- Être tiré à 4 épingles, to be dressed up to the nines, To be stretch by 4
  pins (4 pins to stretch a piece of fabric and eliminate creases)

- Être joli comme un coeur, to be pretty as a picture, Be pretty as an heart.

Fear and worry
--------------

- Serrer les fesses, to have the wind up, tighten the buttocks

- Avoir une peur bleue, to be scared to death, Having blue fear

Directness
----------

- Ne pas y aller par 4 chemins, to go straight to the point, Don't go by 4
  differents tracks.

- Ne pas y aller avec le dos de la cuillère, not to go for half-measures, not
  to dish things out using the back of a spoon.

Speaking and keeping silent
---------------------------

- Tenir la jambe à quelqu'un, to bore someone with one's talk, hold the leg of
  someone (picture someone who wants to go away)

- Souler quelqu'un, to bore someone silly, Make someone drunk.

- Ne pas avoir sa langue dans sa poche, never to be at a loss for words, Don't
  have his tongue in his pocket.

Surprise
--------

- Couper le sifflet à quelqu'un, to shut someone up, Cut the whistle of
  somebody.

Happiness
---------

- Être au anges, to be walking on air, Be with the angels

Sadness
-------

- Avoir le cafard, to feel down, to have the cockroach

- Être malheureux comme les pierres, to be utterly miserable, be as sad as a
  rock.

- Rire comme un bossu/une baleine, to laugh one's head off, It is believed that
  "rire comme une baleine" (to laugh like a whale) might have derived from "se
  tordre comme une baleine de parapluie retourné", playing on the words "se
  tordre", which means both "to bend" and 'to laugh' (in colloquial language)
  and "baleine" which of course means "whale" but which also means the rib of
  an umbrella.

Madness
-------

- Il ne tourne pas rond, he's not all there, he is not turning around.

- Être timbré, to be cracked, to be stamped

- Être tombé sur la tête, to have a screw loose, have fallen on head.

- Être marteau, to be nuts, be a hammer

Stupidity
---------

- Il n'a pas inventé l'eau chaude, he'll never set the Thames on fire, he
  didn't invented warm water.

- Être bête à manger du foin, to be as thick as two shorts planks, Be stupid
  enough to eat hay

- Être bête comme ses pieds, to be unbelievably stupid, Be as stupid as his own
  feet.

- Avoir une case de vide, to have a screw loose, have an empty shake

- Il est un peu bas de plafond, he's not the sharpest knife in the drawer, his
  ceiling is quite low

Hunger and eating
-----------------

- Avoir les crocs, to be famished, got the fangs

Drinking
--------

- Boire comme un trou, to drink like a fish, drink like a hole

- Avoir la gueule de bois, to be hangover, having a wooden face

- Avoir du vent dans les voiles, to be three sheets in the wind, Having wind in
  the sails. This expression likens the teetering drunkard to a ship that is
  buffeted by the wind.

- Lever le coude, to booze, lift the elbow

- S'en jeter un derrière la cravate, to knock back a drink, to throw one behind
  the tie

Doing things easily
-------------------

- Ce n'est pas la mer à boire, it's no big deal, it's not the sea to drink

- Faire quelque chose les doigts dans le nez, to do something very easily, do
  something with fingers in the nose.

- Ce n'est pas le bout du monde, it's no big deal, it's not the end of the
  world

Leaving
-------

- Prendre ses jambes à son cou, to take one's heels, take his legs to his neck.

- Mettre les voiles, to skedaddle, to set up the sails

- Se faire la malle, to clear off, to make the trunk (big suitcase)

Work and effort
---------------

- Être un bourreau de travail, being a workaholic, to be an work executioner

- Faire des pieds et des mains pour faire quelque chose, to move heaven and
  earth to do something, move feet and hands to do something.

- Avoir un poil dans la main, to be workshy, having an hair in the hand.

Success
-------

- Marcher comme sur des roulettes, to be going very smoothly, work on roulette
  (little wheels)

Inferiority
-----------

- Ses rivaux ne lui arrivent pas à la cheville, non of his rivals can touch
  him, his rivals doesn't even height match his anckles.

- ça ne vaut pas un clou, it's not worth a bean, it doesn't worth a nail

Authority and obediance
-----------------------

- Mener quelqu'un à la baguette, to rule someone with a rod of iron, lead
  someone with a stick

Enjoying oneself
----------------

- S'en payer une tranche, to have a whale of a time, to pay one's slice

- s'éclater comme une bête, to have a wild time, to bust like a beast

Money
-----

- Couter la peau des fesses, to cost an arm and a leg, to cost skin of butt

- Jeter l'argent par les fenêtres, to throw money down the drain, to throw
  money through the windows.

- Être plein aux as, to be rolling it, to be full to aces.

- Être payé au lance-pierre, to be paid peanuts, to be paid by slingshot
