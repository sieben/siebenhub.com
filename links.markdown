---
title: Interesting Links
layout: page
---

association
-----------

- [Paris Montagne](//paris-montagne.org) The association I'm a
  volunteer in. Check us out, we do science, try to hack education
  and it's awesome! :)


research
---------

- [10 Things I Wish My Advisor Had Told
  Me](//www-net.cs.umass.edu/kurose/talks/student_keynote_final.pdf),
student workshop keynote by J.Kurose at IEEE INFOCOM'09 and ACM
CoNEXT'09

- [Top-10 Tips for Writing a
  Paper](//www-net.cs.umass.edu/kurose/talks/top_10_tips_for_writing_a_paper.ppt),
student workshop keynote by J.Kurose at ACM CoNEXT 2006.

- [How to read a paper](//portal.acm.org/citation.cfm?id=1273458), by S.
  Keshav, In ACM SIGCOMM CCR

- [How not to review a paper](//portal.acm.org/citation.cfm?id=1519122),
  by G. Cormode, In ACM SIGMOD Record

- [How to increase the chances your paper is accepted at ACM
  SIGCOMM](//sigcomm.org/for-authors/hints-tips-and-guides/author-guide),
by C.  Partridge, IN ACM SIGCOMM CCR

- [You and Your
  Research](//www.cs.virginia.edu/~robins/YouAndYourResearch.html),
transcript of a seminar by R. W. Hamming, March 7th 1986

- Further [resources on
  academia](//www2.cs.utah.edu/~wilson/academia.html) and [advices on
research](//www.cs.cmu.edu/afs/cs.cmu.edu/user/mleone/web/how-to.html)
and writing you may want to check... and, oh, yes,
[http://www.phdcomics.com/](http://www.phdcomics.com/)


formation & academia
--------------------

- [ENSIIE](//www.ensiie.fr) My cs-engineering school.
- [UPCM](//upmc.fr) My cs-university.

cs & software
---------------

- [Hackers News](//news.ycombinator.com) My main source of information
- [Dochub.io](//dochub.io) All documentation I use when I code
  HTML/CSS/JS

humor 
------

- [XKCD](//xkcd.com) g33k & science humor
- [Dilbert](//dilbert.com) engineer humor
