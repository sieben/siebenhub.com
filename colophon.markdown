---
title: Colophon
layout: page
---

Every line of the code and articles that make this website what it is
was written using [vim](http://www.vim.org/). The article are written
using Markdown as file format. This website use
[Jekyll](http://jekyllrb.com/) to generate static content and is hosted
thanks to [github](http://github.com).

I currently use [Ubuntu](http://ubuntu.com) Precise Pangolin as
operating system on my Dell E6410 and I'm quite happy about it.

You can subscribe to my blog using the [atom feed](/atom.xml).

This complete website is open source and can be
[forked](//github.com/sieben/sieben.github.com/).
