---
title: Ring list
layout: page
---

Titanium Kay
------------

- [Classical](//www.titaniumkay.com/CLASSICAL-Tungsten-Carbide-Mens-Plain-Dome-Wedding-Band-P102265.html)
- [Titanic](http://www.titaniumkay.com/TITANIC-Mens-Tungsten-Carbide-Wedding-Ring-P102252.html)

Larson Jewelers
---------------

- [Cronus](http://www.larsonjewelers.com/p-341-cronus-brushed-center-with-polished-bevels-tungsten-wedding-band-6mm-8mm.aspx)
- [Nashville](http://www.larsonjewelers.com/p-146-nashville-domed-tungsten-ring-by-benchmark.aspx)
- [Magnus](http://www.larsonjewelers.com/p-277-magnus-domed-brush-finished-tungsten-carbide-ring-with-dual-grooves-6mm-8mm.aspx)
- [Boss](http://www.larsonjewelers.com/p-284-boss-domed-center-groove-tungsten-carbide-ring-with-brush-finish-6-8-mm.aspx)
- [Arlington](http://www.larsonjewelers.com/p-630-arlington-domed-white-tungsten-ring-for-men-2mm-12mm.aspx)
- [Dominus](http://www.larsonjewelers.com/p-126-dominus-domed-tungsten-carbide-ring-4-12-mm.aspx)
- [Bridgeport](http://www.larsonjewelers.com/p-70-bridgeport-flat-satin-finish-tungsten-carbide-ring-6mm-10mm.aspx)
