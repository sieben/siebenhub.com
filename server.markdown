---
title: Sieben-server config
layout: page
---

I would like information on the following configuration. I'm trying to
find a setup to upgrade my current desktop to a nicer configuration. I
already got my screen, computer case.

Use cases: 

- Very casual gaming (RPG like Oblivion or FPS like UT2004)
> I don't want to buy an expensive graphical card. Something integrated
> to the motherboard would be nice.
- Massive storing (From 1 To to 3 To)


Current list:
=============

- [Kingston ValueRAM 8 Go DDR3 1333 MHz CL9](//ldlc.com/fiche/PB00131602.html)
- [TP-LINK TL-WN881ND](//ldlc.com/fiche/PB00126311.html)
- [Gigabyte GA-Z77-D3H](//ldlc.com/fiche/PB00128846.html)
- [Intel Core i3-3225 (3.3 GHz)](//ldlc.com/fiche/PB00135306.html)

Options
=======

- [Noctua NH-U12P SE2](//ldlc.com/fiche/PB00092228.html)
